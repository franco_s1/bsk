package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList<Frame> frames;
	private int firstBonus;
	private int secondBonus;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonus = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonus = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int total = 0;
		for (int i = 0; i < frames.size(); i++) {
			if (frames.get(i).isSpare()) {
				spare(i);
			} else if (frames.get(i).isStrike()) {
				strike(i);
			}
			total += frames.get(i).getScore();
		}
		return total;
	}

	public void spare(int i) {
		if (!((i + 1) < frames.size())) {
			frames.get(i).setBonus(getFirstBonusThrow());
		} else {
			frames.get(i).setBonus(frames.get(i + 1).getFirstThrow());
		}
	}

	public void strike(int i) {
		if (!((i + 1) < frames.size())) {
			frames.get(i).setBonus(getFirstBonusThrow() + getSecondBonusThrow());
		} else {
			anotherStrike(i);
		}
	}

	public void anotherStrike(int i) {
		if (frames.get(i + 1).isStrike()) {
			if ((i + 2) < frames.size()) {
				frames.get(i).setBonus(frames.get(i + 1).getScore() + frames.get(i + 2).getFirstThrow());
			} else {
				frames.get(i).setBonus(getFirstBonusThrow() + getSecondBonusThrow());
			}
		} else {
			frames.get(i).setBonus(frames.get(i + 1).getScore());
		}
	}

	public int getFramesSize() {
		return frames.size();
	}

}
